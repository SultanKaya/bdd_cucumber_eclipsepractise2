package step_definitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import utilities.BrowserUtils;

import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.util.concurrent.TimeUnit;

public class Hook {
	
	

    private static Logger log = Logger.getLogger(Hook.class);

    @Before
    public void setUp() {
        utilities.Driver.getDriver().manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
        // Driver.getDriver().manage().window().maximize();
    }

    @After
    public void tearDown(Scenario scenario) {
        if (scenario.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot)utilities.Driver.getDriver()).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
        }

	     BrowserUtils.waitFor(5);
	   utilities.Driver.closeDriver();

	    }
}
