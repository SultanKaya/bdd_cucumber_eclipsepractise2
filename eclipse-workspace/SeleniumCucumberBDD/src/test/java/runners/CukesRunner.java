package runners;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
 
@RunWith(Cucumber.class)
@CucumberOptions(
		features="resources/features", 
		glue="",
		 plugin = {
	                "pretty",
	                "html:test-output/ignite-report",
	                "json:test-output/cucumber.json",
	                "rerun:test-output/rerun.txt"
	        },
		 tags =" @regressionTest",

	        dryRun = true
		 )

public class CukesRunner {

}
